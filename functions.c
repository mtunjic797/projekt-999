#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "header.h"




int unos()
{

	FILE* fileP = NULL;
	FILM* unosFilma = NULL;
	unosFilma = (FILM*)malloc(sizeof(FILM));
	int brojFilmova;
	if (unosFilma == NULL)
	{
		return 1;
	}
	else
	{
		printf("Unesite ime filma: ");
		scanf(" %49[^\n]", unosFilma->imeFilma);
		printf("Unesite ime redatelja: ");
		scanf(" %29[^\n]", unosFilma->redateljFilma);
		printf("Unesite zanrFilma: ");
		scanf(" %29[^\n]", unosFilma->zanrFilma);
		printf("Unesite godinu filma: ");
		scanf("%d", &unosFilma->godinaFilma);
		fileP = fopen("filmovi.bin", "rb");
		if (fileP == NULL) 
		{
			fileP = fopen("filmovi.bin", "wb");
			brojFilmova = 1;
			fwrite(&brojFilmova, sizeof(int), 1, fileP);
			fwrite(unosFilma, sizeof(FILM), 1, fileP);
			fclose(fileP);
		}
		else
		{
			fclose(fileP);
			fileP = fopen("filmovi.bin", "rb+");
			if (fileP == NULL)
			{
				return 1;
			}
			else 
			{
				fseek(fileP, 0, SEEK_SET);
				fread(&brojFilmova, sizeof(int), 1, fileP);
				brojFilmova++;
				fseek(fileP, 0, SEEK_SET);
				fwrite(&brojFilmova, sizeof(int), 1, fileP);
				fseek(fileP, 0, SEEK_END);
				fwrite(unosFilma, sizeof(FILM), 1, fileP);
				fclose(fileP);
			}
		}
	}

	free(unosFilma);
	return 0;
}

int ispis(FILM* film)
{
	{
		printf("Ime %49s\t%p\n", film->imeFilma, &film->imeFilma);
		printf("Godina %d\t%p\n", film->godinaFilma, &film->godinaFilma);
		printf("Redatelj %29s\t%p\n", film->redateljFilma, &film->redateljFilma);
		printf("Zanr %29s\t%p\n", film->zanrFilma, &film->zanrFilma);
		printf("Ocjena %d\t%p\n", film->ocjenaFilma, &film->ocjenaFilma);
		printf("Opis %149s\t%p\n", film->opisFilma, &film->opisFilma);
	}
	return;
}





void pretragaFilmova(char kriterij)
{
	FILM* filmovi = NULL;
	FILE* fileP = NULL;
	char trazenoIme[30], trazeniRedatelj[20], trazeniZanr[20];
	int trazenaGodina, trazenaOcjena;

	int brojFilmova, f = 0;
	system("cls");

	fileP = fopen("filmovi.bin", "rb");
	if (fileP == NULL)
	{
		printf("\nGreska.\n");
	}
	else
	{
		fread(&brojFilmova, sizeof(int), 1, fileP);
		filmovi = (FILM*)malloc(sizeof(FILM));
		if (filmovi == NULL)
		{
			printf("Greska.\n");
		}
		else
		{
			switch (kriterij)
			{
			case 'I':
				printf("Unesite ime trazenog filma: ");
				scanf(" %49[^\n]", &trazenoIme);
				for (int i = 0; i < brojFilmova; i++)
				{
					fread(filmovi, sizeof(FILM), 1, fileP);
					if (strcmp(filmovi->imeFilma, trazenoIme) == 0)
					{
						printf("\nFilm je dostupan.");
						ispis(filmovi);
						f = 1;
						break;
					}
				}
				break;

			case 'G':
				printf("Unesite godinu trazenog filma: ");
				scanf(" %d", &trazenaGodina);
				for (int i = 0; i < brojFilmova; i++) 
				{
					fread(filmovi, sizeof(FILM), 1, fileP);
					if (filmovi->godinaFilma == trazenaGodina)
					{
						printf("\nFilm je dostupan.");
						ispis(filmovi);
						f = 1;
					}
				}
				break;

			case 'R':
				printf("Unesite redatelja trazenog filma: ");
				scanf(" %29[^\n]", &trazeniRedatelj);
				for (int i = 0; i < brojFilmova; i++)
				{
					fread(filmovi, sizeof(FILM), 1, fileP);
					if (strcmp(filmovi->redateljFilma, trazeniRedatelj) == 0)
					{
						printf("\nFilm je dostupan.");
						ispis(filmovi);
						f = 1;
					}
				}
				break;

			case 'Z':
				printf("Unesite zanr trazenog filma: ");
				scanf(" %29[^\n]", &trazeniZanr);
				for (int i = 0; i < brojFilmova; i++)
				{
					fread(filmovi, sizeof(FILM), 1, fileP);
					if (strcmp(filmovi->zanrFilma, trazeniZanr) == 0)
					{
						printf("\nFilm je dostupan.");
						ispis(filmovi);
						f = 1;
					}
				}
				break;
			case 'O':
				printf("Unesite ocjenu trazenog filma: ");
				scanf(" %d\n", &trazenaOcjena);
				for (int i = 0; i < brojFilmova; i++)
				{
					fread(filmovi, sizeof(FILM), 1, fileP);
					if (strcmp(filmovi->ocjenaFilma, trazenaOcjena) == 0)
					{
						printf("\nFilm je dostupan.");
						ispis(filmovi);
						f = 1;
					}
				}
				break;
			}
			if (f == 0) 
			{
				printf("\nFilm nije dostupan!");
			}
			fclose(fileP);
		}
	}
}

void nacinPretrage()
{
	system("cls");
	printf("1. Pretrazivanje po imenu\n");
	printf("2. Pretrazivanje po godini\n");
	printf("3. Pretrazivanje po redatelju\n");
	printf("4. Pretrazivanje po zanru\n");
	printf("5. Pretrazivanje po ocjeni\n");
	printf("0. Povratak u glavni izbornik\n");
	char odabir = getch();
	switch (odabir) 
	{
	case '1':
		pretragaFilmova('I');
		break;

	case '2':
		pretragaFilmova('G');
		break;

	case '3':
		pretragaFilmova('R');
		break;

	case '4':
		pretragaFilmova('Z');
		break;

	case '5':
		pretragaFilmova('O');
		break;

	case '0':
		return;
		break;
	default:
		printf("\nPogresan unos!");
	}
}


void izmjenaPodataka()
{

	FILE* fileP = NULL;
	FILM* film = NULL;
	int brojFilmova;
	fileP = fopen("filmovi.bin", "rb+");
	if (fileP == NULL)
	{
		printf("\nGreska!");
	}
	else
	{
		fread(&brojFilmova, sizeof(int), 1, fileP);
		film = (FILM*)malloc(sizeof(FILM));
		if (film == NULL)
		{
			printf("\nGreska!");
		}
		else
		{
			char trazenoIme[30];
			int f = 0;
			system("cls");
			printf("Unesite ime filma kojemu zelite izmjeniti podatke: ");
			scanf(" %49[^\n]", trazenoIme);
			for (int i = 0; i < brojFilmova; i++)
			{
				fread(film, sizeof(FILM), 1, fileP);
				if (strcmp(film->imeFilma, trazenoIme) == 0)
				{
					f = 1;
					ispis(film);
					system("cls");
					printf("1. Promjena imena filma");
					printf("\n2. Promjena imena redatelja");
					printf("\n3. Promjena zanra");
					printf("\n4. Promjena godine\n");
					printf("\n5. Promjena ocjene\n");
					printf("\n6. Promjena opisa\n");
					printf("7. Povratak u glavni izbornik\n");
					char odabir = getch();
					switch (odabir)
					{
					case '1':
						printf("\nUnesite novo ime: ");
						char novoIme[30];
						scanf(" %49[^\n]", novoIme);
						strcpy((film)->imeFilma, novoIme);
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("Ime je uspjesno promjenjeno.\n");
						break;

					case '2':
						printf("\nUnesite novog redatelja: ");
						char noviRedatelj[50];
						scanf(" %29[^\n]", noviRedatelj);
						strcpy((film)->redateljFilma, noviRedatelj);
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("\nRedatelj je uspjesno promjenjen\n");
						break;

					case '3':
						printf("\nUnesite novi zanr filma: ");
						char noviZanr[20];
						scanf(" %29[^\n]", noviZanr);
						strcpy((film)->zanrFilma, noviZanr);
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("\nZanr uspjesno promjenjen.\n");
						break;

					case '4':
						printf("\nUnesite novu godinu: ");
						int novaGodina;
						scanf("%d", &novaGodina);
						film->godinaFilma = novaGodina;
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("\nGodina uspjesno promjenjena.\n");
						break;

					case '5':
						printf("\nUnesite novu ocjenu: ");
						int novaOcjena;
						scanf("%d", &novaOcjena);
						film->ocjenaFilma = novaOcjena;
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("\nOcjena uspjesno promjenjena.\n");
						break;
					case '6':
						printf("\nUnesite novi opis filma: ");
						char noviOpis[150];
						scanf("%149[^\n]", noviOpis);
						strcpy((film)->opisFilma, noviOpis);
						fseek(fileP, -(int)sizeof(FILM), SEEK_CUR);
						fwrite(film, sizeof(FILM), 1, fileP);
						printf("\nOpis je uspjesno promjenjen.\n");
						break;

					case '7':
						return;
						break;
					default:
						printf("Pogresan unos!");
					}
					break;
				}
			}

			if (f == 0)
			{
				printf("\nFilm nije pronaden!");
			}
		}
		fclose(fileP);
	}
	free(film);
	film = NULL;
}

int noviIspis()
{
	int broj;
	FILM* filmovi = NULL;
	FILE* fileP = NULL;
	fopen("filmovi.bin", "rb");
	if (fileP == NULL) 
	{
	return 1;
	}
	else
	{

		fread(&broj, sizeof(int), 1, fileP);

		filmovi = (FILM*)malloc(sizeof(FILM));

		if (filmovi == NULL)
		{
			return 1;
		}
		else 
		{
	
			printf("U videoteci ima %d filmova%s.", broj, broj == 1 || broj > 4 ? "a" : "e");
	
			for (int i = 0; i < broj; i++)
			{
				fread(filmovi, sizeof(FILM), 1, fileP);
				ispis(filmovi);
			}
			free(filmovi);
			fclose(fileP);
			return broj;
		}
	}
}




void brisanjeFilma()
{
	FILE* fileP = NULL;
	FILM* film = NULL;
	int brojFilmova;
	int delete;
	fileP = fopen("filmovi.bin", "rb");
	if (fileP == NULL) 
	{
		printf("\nGreska!");
	}
	else {
		fread(&brojFilmova, sizeof(int), 1, fileP);
		film = (FILM*)malloc(brojFilmova * sizeof(FILM));

		if (film == NULL) 
		{
			printf("\nGreska!");
		}
		else 
		{
			fread(film, sizeof(FILM), brojFilmova, fileP);
			fclose(fileP);
			char trazenoIme[50];
			int f = 0;
			system("cls");
			printf("Unesite ime filma koji zelite odabrati!");
			scanf(" %49[^\n]", trazenoIme);
			for (int i = 0; i < brojFilmova; i++)
			{
				if (strcmp((film + i)->imeFilma, trazenoIme) == 0) 
				{
					ispis(film + i);
					delete = i;
					f = 1;
					break;
				}
			}
			if (f == 0) 
			{
				printf("\nFilm nije pronaden!");
			}
			else 
			{
				fclose(fileP);
				fileP = fopen("filmovi.bin", "wb");
				if (fileP == NULL)
				{
					printf("\nGreska!");
				}
				else 
				{
					brojFilmova--;
					fwrite(&brojFilmova, sizeof(int), 1, fileP);
					for (int i = 0; i < brojFilmova + 1; i++) 
					{
						if (i == delete)
						{
							continue;
						}
						else
						{
							fwrite((film + i), sizeof(FILM), 1, fileP);
						}
					}
					printf("\n\nFilm je uspjesno obrisan!");
					fclose(fileP);
				}
			}
		}
	}
}



void izbornik()
{

	int odabir;

	while (1)
	{
		system("cls");
		printf("\n   ================================================\n\n");
		printf("                   Evidencija filmova\n");
		printf("\n   ================================================\n\n");
		printf("          1. Ispis filmova na listi.\n");
		printf("          2. Unos novih filmova na listu.\n");
		printf("          3. Pretraga filmova po nazivima.\n");
		printf("          4. Izmijenjivanje podataka o filmu.\n");
		printf("          5. Brisanje filmova\n");
		printf("          0. Kraj programa\n");

		printf("          Odaberite jednu od ponudenih opcija: ");

		odabir = getch();
		switch (odabir)
		{
		case 1:
			system("cls");
			if (noviIspis() == -1) 
			{
				printf("\nGreska.\n");
			}
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;

		case 2:
			system("cls");
			if (unos() == 1) 
			{
				printf("\nGreska.\n");
			}
			printf("\n\nPritisnite bilo koju tipku za povratak...");
			getch();
			break;


		case 3:
			nacinPretrage();
			printf("\n\nPritisnite bilo koju tipku za povratak!");
			getch();
			break;

		case 4:
			izmjenaPodataka();
			printf("\n\nPritisnite bilo koju tipku za povratak!");
			getch();
			break;

		case 5:
			brisanjeFilma();
			printf("\n\nPritisnite bilo koju tipku za povratak!");
			getch();
			break;

		case 0:
			system("cls");
			izlazakIzPrograma();
		default:
			printf("\nKrivo odabrana opcija, molimo pokusajte ponovno!\n");
		}
	}
}


void izlazakIzPrograma(void)
{
	printf("\nJeste li ste sigurni da zelite zavrsiti program? [DA|NE]:");
	char odabir[3] = { '\0' };
	scanf(" %2s", &odabir);
	if (!strcmp("DA", odabir))
	{
		exit(EXIT_FAILURE);
	}
	return;
}
