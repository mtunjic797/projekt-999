#define _CRT_SECURE_NO_WARNINGS

#ifndef HEADER_H
#define HEADER_H


typedef struct filmovi
{
	int godinaFilma;
	char imeFilma[50];
	char redateljFilma[30];
	char zanrFilma[30];
	int ocjenaFilma;
	char opisFilma[150];

}FILM;


void izbornik();
int unos();
int ispis(FILM*);
void pretragaFilmova(char);
void nacinPretrage();
void izmjenaPodataka();
int noviIspis();
void brisanjeFilma();
void izlazakIzPrograma();

#endif

